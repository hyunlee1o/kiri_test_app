"""
This file contains the functional tests for the module

"""


def test_home_page_get(test_client):
    """
    Tests if the endpoints are reachable through a get.
    / should return a '302' as it is a redirect
    /data should return a 200
    """
    response = test_client.get("/")
    assert response.status_code == 302
    response = test_client.get("/data")
    assert response.status_code == 200


def test_home_page_post(test_client):
    """
    / should return a '405' status code as it's not allowed
    /data should return a '200' status code
    """
    response = test_client.post("/")
    assert response.status_code == 405
    response = test_client.post("/data")
    assert response.status_code == 200


def test_data(test_client):
    """
    Tested the page_size argument if it returns more items than expected.
    """
    response = test_client.post("/data", json={"start": 0, "page_size": 1})
    assert response.status_code == 200
    assert len(response.json) == 1
    response = test_client.post("/data", json={"start": 1000, "page_size": 2})
    assert response.status_code == 200
    assert len(response.json) == 2
    response = test_client.post("/data", json={"start": 2000, "page_size": 20})
    assert response.status_code == 200
    assert len(response.json) == 20
    response = test_client.post("/data", json={"start": 4000, "page_size": 21})
    assert response.status_code == 200
    assert len(response.json) == 20


def test_elasticsearch():
    """
    Brute forcing if elasticsearch is available
    """
    from elasticsearch import Elasticsearch

    es = Elasticsearch()
    assert es is not None
