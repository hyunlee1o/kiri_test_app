import elasticsearch


def test_elasticsearch():
    """
    Tests if we can add and remove items into elastic
    """

    es = elasticsearch.Elasticsearch()
    assert es is not None
    index = "kiri_test"
    es.indices.create(index=index)
    es.indices.put_mapping(
        body={
            "properties": {
                "id": {"type": "keyword"},
                "type": {
                    "type": "text",
                    "fields": {"keyword": {"type": "keyword"}},
                },
            }
        },
        index=index,
    )
    es.create(index, "1", {"id": "1", "type": index}, refresh=True)
    res = es.get(id="1", index=index)
    assert res is not None
    assert res.get("_source").get("type") == "kiri_test"
    es.delete(index=index, id="1")
    es.indices.delete(index=index)
