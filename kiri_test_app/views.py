"""This module contains the pagination endpoints."""

from flask import render_template, url_for, redirect, request, jsonify, current_app
from elasticsearch import Elasticsearch

global es
es = Elasticsearch()


@current_app.route("/")
def index():
    """
    Default endpoint, just in case it redirects to the /data endpoint
    """
    return redirect(url_for("data"))


@current_app.route("/data", methods=["GET", "POST"])
def data():
    """
    Pagination endpoint.
    It allows almost any kind of simple input, both int and string are allowed,
    when asking the endpoint for elasticsearch documents.

    Return:
    ------
        Json or HTML
        Json input gets json answered and else you get a template rendered.
    """
    default_page_size = 20
    default_start = 0
    if request.is_json:
        json_data = request.get_json()
        start = json_data.get("start", default_start)
        page_size_ = json_data.get("page_size", default_page_size)
    else:
        if request.method == "GET":
            start = request.args.get("start", default_start, type=int)
            page_size_ = request.args.get("page_size", default_page_size, type=int)
        elif request.method == "POST":
            start = request.values.get("start", default_start, type=int)
            page_size_ = request.values.get("page_size", default_page_size, type=int)
    try:
        if not isinstance(start, int):
            start = int(start)
        if not isinstance(page_size_, int):
            page_size_ = int(page_size_)
    except TypeError:
        return f"Bad request, {start=} {page_size_=}", 400
    page_size = default_page_size if page_size_ > default_page_size else page_size_
    res = es.search(index="stores", from_=start, size=page_size)
    stores = [item.get("_source") for item in res.get("hits").get("hits")]
    if request.is_json:
        return jsonify(stores)
    return render_template(
        "index.html", stores=stores, starting_id=start + 1, page_size=page_size
    )
