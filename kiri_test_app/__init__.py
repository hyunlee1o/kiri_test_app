""" This is the init module for kiri_test"""
import csv
from pathlib import Path
import chardet
from flask import Flask
from elasticsearch import helpers, Elasticsearch, NotFoundError

global es
es = Elasticsearch()


def create_app(config_file=None):
    """This is our app factory.
    It inits the app from our config and later it imports the views.
    """
    app = Flask(__name__)
    app.config.from_object("config.default")
    app.config.from_pyfile("config.py")
    with app.app_context():
        import kiri_test_app.views
    init_elasticsearch_index()
    return app


def init_elasticsearch_index():
    """Create the stores database in case stores doesn't exist"""
    try:
        es.indices.get("stores")
    except NotFoundError:
        create_stores_database()


def create_stores_database():
    """
    Here we create the stores database in case it doesn't exist.
    First we get the type of encoding the file has and later we read the
    file columns so we can put them correctly into elasticsearch.
    Finally they get added after creating the index.
    """
    fieldnames = {
        "id": {"type": "keyword"},
        "inventory name": {"type": "keyword"},
        "contact name": {"type": "keyword"},
        "stock": {"type": "long"},
        "last revenue": {"type": "double"},
        "current revenue": {"type": "double"},
        "refund": {"type": "double"},
        "company name": {"type": "keyword"},
        "categories": {"type": "keyword"},
        "rating": {"type": "double"},
    }
    file_path = Path("kiri_test_app/static/SampleCSVFile_556kb.csv")
    with open(file_path, "rb") as f:
        reader = f.read()
        encoding = chardet.detect(reader).get("encoding")  # windows-1252

    with open(file_path, encoding=encoding) as f:
        reader = csv.DictReader(f, fieldnames=[*fieldnames])
        index = "stores"
        es.indices.create(
            index=index,
            body={
                "settings": {"number_of_shards": 1},
                "mappings": {
                    "properties": fieldnames,
                },
            },
        )
        helpers.bulk(es, reader, index=index)
