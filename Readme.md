## About The Project

![Sample image of the project working](https://bitbucket.org/hyunlee1o/kiri_test_app/raw/40216500207938265e7bd952b88a723cde500efa/data_sample.png "pagination")

This is the test made for the kiri technologies testing part of the interview.
It's basically an api to query elasticsearch.

### Built With

* [Python](https://python.org)
* [flask](https://flask.palletsprojects.com/en/2.0.x/)
* [elasticsearch](https://www.elastic.co/elasticsearch/)



## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* python3
* poetry
  ```sh
  curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
  ```
* docker-compose
* docker

### Installation

1. Clone the repo
   ```sh
   git clone https://bitbucket.org/hyunlee1o/kiri_test_app.git
   ```
2. Install the packages and dependencies
   ```sh
   poetry install
   docker-compose up -d
   ```


## Usage

To run this project use this after installing the dependencies
   ```sh
   flask run
   ```
Then it will be available at http://localhost:5000 in your browser

You can query using your browser url GET like localhost:5000?start=0&page_size=10 to get the first 10 documents
of the elasticsearch stores index

You can test the server while it's running with requests such as:
	```sh
	curl -XPOST "http://localhost:5000/data" -d "{"start":0,"size":1}"
	
	curl -XGET "http://localhost:5000/data" -d "start=0&size=1"
	
	curl -XPOST "http://localhost:5000/data" -d '{"start":0,"page_size":"1"}' -H 'Content-Type: application/json'
	```

At least run the app once before testing because it inits the elasticsearch database.
To run the tests use:
   ```sh
   python -m pytest -v
   ```



## License

Distributed under the MIT License. See `LICENSE` for more information.



## Contact

Project Link: [https://bitbucket.org/hyunlee1o/kiri_test_app/](https://bitbucket.org/hyunlee1o/kiri_test_app/)
